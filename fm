#!/bin/bash
PS3="Select an action(1-6): "
ods="OFF" # only directory default status
sds="ON"	# show directory default status
llfs="OFF" # long list format default status
afs="OFF" # show all files default status
fns="ON"	# file numeration default status

llfc=""	# long list format command
ss="none" # sort status
sfc="-U" # sort file command
fnc="| nl -w 1 -s ') '" # file number command

watch_files_com="ls . -1 $sfc $fnc" # command for ls_pro function
source_file_com="ls . -1 $sfc $fnc" # command for number to source

exit_var=0
exit_var_ch=0
sort_type=0

help(){
	echo "Usage: ./fm [-d|--directory <path_to_directory>] [-h|--help]
	[--mode < -od -llf -af -dfn -dsd > mode--]

	-d|--directory <path_to_directory>
			starting with this directory

	-h|--help
			show this message

	[--mode ... mode--] flags:

		-od 	show only directory
		-llf	long list format(like ls -l see ls --help)
		-af	show all files(including files starting with . )
		-dfn	not-show directory
		-dsd	do not number files

	Example:
		./fm -d /home
		./fm -d /home --mode -od -llf -af mode--
		./fm --help
		"
}

fun_sort(){ # for choice of sort in change mode
	let sort_type=sort_type+1;
	case $sort_type in
		0) ss="none"; sfc="-U";;
		1) ss="extension"; sfc="-X";;
		2) ss="size"; sfc="-S";;
		3) ss="time"; sfc="-t";;
		4) ss="version"; sfc="-v";;
		5) ss="revers"; sfc="-r";;
		*) let sort_type=0; ss="none"; sfc="-U";;
	esac
}

function param_select(){ # select parameters from command lines
	for param in "$@"
	do
		case $param in
			"-d"|"--directory") let value=1; continue;;
			"-h"|"--help") help; exit 0;;
			"--mode") let value=2; continue;;
			"mode--") let value=0; continue;;
			*);;
		esac
		case $value in
			1) cd "$param"; echo `pwd`; let value=0; continue;;
			2)
			case $param in
				"-od") ods="ON"; sds="OFF"; continue;; # only directory
				"-llf") llfs="ON"; llfc="-l"; continue;; # long list format
				"-af") afs="ON"; afc="-a"; continue;; # all file visible
				"-dfn") fns="OFF"; fnc=""; continue;; # disable file numeration
				"-dsd") sds="OFF"; ods="OFF"; continue;; # disable show directory
				*);;
			esac;;
			*);;
		esac
	done
}

ls_pro(){ # function for print overview
	clear
	echo
	echo "########################### OVERVIEW ###########################"
	echo
	eval $watch_files_com
	echo
	echo "################################################################"
	echo "############################# MENU #############################"
}

watch_files_com_gen(){ # function for command generation
	if [ "$ods" = "ON" ]
	then
		if [ "$afs" = "ON" ]
		then
			watch_files_com="ls . -1 $llfc $sfc -d */ .*/ | cut -f1 -d'/' $fnc"
			source_file_com="ls . -1 $sfc -d */ .*/ | cut -f1 -d'/' $fnc"
		else
			watch_files_com="ls . -1 $llfc $sfc -d */ | cut -f1 -d'/' $fnc"
			source_file_com="ls . -1 $sfc -d */ | cut -f1 -d'/' $fnc"
		fi
	elif [ "$sds" = "OFF" ]
	then
		if [ "$afs" = "ON" ]
		then
			watch_files_com="find . -maxdepth 1 -type f -exec ls . -d $llfc $sfc -1 {} + | sed 's/.\///' $fnc"
			source_file_com="find . -maxdepth 1 -type f -exec ls . -d $sfc -1 {} + | sed 's/.\///' $fnc"
		else
			watch_files_com="find . -maxdepth 1 \( ! -name '.*' \) -type f -exec ls . -d $llfc $sfc -1 {} + | sed 's/.\///' $fnc"
			source_file_com="find . -maxdepth 1 \( ! -name '.*' \) -type f -exec ls . -d $sfc -1 {} + | sed 's/.\///' $fnc"
		fi
	else
		watch_files_com="ls . -1 $llfc $afc $sfc | sed '/total /d' $fnc"
		source_file_com="ls . -1 $afc $sfc | sed '/total /d' $fnc"
	fi
}

fun_change_mode(){ # function for change display mode
	exit_var_ch=0
	while [ $exit_var_ch -eq 0 ]
	do
		watch_files_com_gen
		clear
		ls_pro
		show_dir="Show(ON)/Not-show(OFF) directory($sds)"
		llf="Long list format($llfs)"
		only_dir="Only directory($ods)"
		sort="Sort($ss)"
		all_files="All files($afs)"
		file_num="File numeration($fns)"
		PS3="Select an action(1-7): "

		select mode in "$only_dir" "$show_dir" "$llf" "$sort" "$all_files" "$file_num" "Save Changes"
		do
			case $mode in
				"$only_dir")
									if [ "$ods" = "OFF" ]
									then
										ods="ON"
										sds="OFF"
									else
										ods="OFF"
									fi
									break;;
				"$show_dir")
									if [ "$sds" = "OFF" ]
									then
										sds="ON"
										ods="OFF"
									else
										sds="OFF"
									fi
									break;;
				"$llf")
									if [ "$llfs" = "OFF" ]
									then
										llfs="ON"
										llfc="-l"
									else
										llfs="OFF"
										llfc=""
									fi
									break;;
				"$sort")
									fun_sort;
									break;;
				"$all_files")
									if [ "$afs" = "OFF" ]
									then
										afs="ON"
										afc="-a"
									else
										afs="OFF"
										afc=""
									fi
									break;;
				"$file_num")
									if [ "$fns" = "OFF" ]
									then
										fns="ON"
										fnc="| nl -w 1 -s ') '"
									else
										fns="OFF"
										fnc=""
									fi
									break;;
				"Save Changes") exit_var_ch=1; break;;
				*) echo "Non-existen choice";;
			esac

		done
	done
	PS3="Select an action(1-6): "
}

main(){
	watch_files_com_gen
	ls_pro
	while [ $exit_var -eq 0 ]
	do
	select command in "Change mode" "Change dir" "Copy" "Move/Rename" "Remove" "Exit"
	do
		case $command in
			"Change mode")
			fun_change_mode
			ls_pro
			break;;
			"Change dir") new_folder="";
				read -p "Enter new directory(or number dir): " new_folder;
				if [[ "$new_folder" = *\) ]] # check var new_folder on template *)
				then
					new_folder=`eval $source_file_com | sed -n "s/$new_folder //p"` # return source name from number
				fi
				cd "$new_folder"
				status_prev_com=$? # set status previous command
				ls_pro
				if [ $status_prev_com -eq 0 ] # check status change dir
				then
					echo "New dir `pwd`"
					echo
	      fi
				break;;

			"Exit") exit_var=1; clear; break;;
			"Remove")rm_file=""; read -p "Enter filename(or number) to remove:" rm_file;
					if [[ "$rm_file" = *\) ]] # check var rm_file on template *)
					then
						rm_file=`eval $source_file_com | sed -n "s/$rm_file //p"` # return source name from number
					fi
						rm -rf "$rm_file"
						status_prev_com=$? # set status previous command
						ls_pro;
						if [ $status_prev_com -eq 0 ] # check status rm file
						then
							echo
							echo "File $rm_file has been deleted!!!"
							echo
						fi
					break;;
			"Copy")	source=""; dest="";	read -p "Enter source and dest filename for copy
			(For example: './source ./dest' or '1) 2)'):" source dest;
							if [[ "$source" = *\) ]] # check var source on template *)
							then
								source=`eval $source_file_com | sed -n "s/$source //p"` # return source name from number
							fi

							if [[ "$dest" = *\) ]] # check var dest on template *)
							then
								dest=`eval $source_file_com | sed -n "s/$dest //p"` # return source name from number
							fi

							cp -r "$source" "$dest"
							status_prev_com=$? # set status previous command
							ls_pro;
							if [ $status_prev_com -eq 0 ] # check status copy files
							then
								echo
								echo "File $source has been copied to $dest"
								echo
							fi
							break	;;
			"Move/Rename")source=""
					dest=""
					read -p "Enter source and dest filename for move/rename
					(For example: './source ./dest' or '1) 2)'): " source dest
					if [[ "$source" = *\) ]] # check var source on template *)
					then
						source=`eval $source_file_com | sed -n "s/$source //p"` # return source name from number
					fi

					if [[ "$dest" = *\) ]] # check var dest on template *)
					then
						dest=`eval $source_file_com | sed -n "s/$dest //p"` # return source name from number
					fi

					mv -f "$source" "$dest"
					status_prev_com=$? # set status previous command
					ls_pro;
					if [ $status_prev_com -eq 0 ] # check status move/rename file
					then
						echo
						echo "File $source has been moved to $dest"
						echo
					fi
					break;;
			*) echo "Non-existen choice";;
		esac
	done
	done
}

param_select $@
main
