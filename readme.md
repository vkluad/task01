# Simple file manager on bash
---
```
Usage: ./fm [-d|--directory <path_to_directory>] [-h|--help]
[--mode < -od -llf -af -dfn -dsd > mode--]

-d|--directory <path_to_directory>
    starting with this directory

-h|--help
    show this message

[--mode ... mode--] flags:

  -od 	show only directory
  -llf	long list format(like ls -l see ls --help)
  -af   show all files(including files starting with . )
  -dfn	not-show directory
  -dsd	do not number files

Example:
  ./fm -d /home
  ./fm -d /home --mode -od -llf -af mode--
  ./fm --help
  ```
---
